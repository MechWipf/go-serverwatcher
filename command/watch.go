package command

import (
	"local/mechwipf/serverwatcher/watcher"
	"log"
	"os/signal"
	"runtime"

	"flag"

	"os"

	"io/ioutil"

	"github.com/AsynkronIT/gam/actor"
	"github.com/AsynkronIT/gam/remoting"
	"github.com/mitchellh/cli"
	"google.golang.org/grpc"
	"google.golang.org/grpc/grpclog"
)

// WatchCommand for whatever
type WatchCommand struct {
	UI cli.Ui
}

// Run something
func (c *WatchCommand) Run(args []string) int {
	var configPath string
	var config *watcher.Config
	var wait chan int

	runtime.GOMAXPROCS(runtime.NumCPU())

	grpclog.SetLogger(log.New(ioutil.Discard, "[GRPC-Log] ", 0))
	log.SetOutput(ioutil.Discard)

	f := flag.NewFlagSet("watch", flag.ContinueOnError)
	f.StringVar(&configPath, "config", "", "")
	f.Parse(args)

	c.UI.Output("Loading Config")
	config, err := watcher.LoadConfig(configPath)
	if err != nil {
		c.UI.Error("Parsing config failed: " + err.Error())
		return 0
	}

	decider := func(child *actor.PID, reason interface{}) actor.Directive {
		c.UI.Output("handling failure for child")
		return actor.StopDirective
	}

	supervisor := actor.NewOneForOneStrategy(3, 1000, decider)

	svProp := actor.FromInstance(&watcher.WatchSupervisorActor{UI: c.UI, Wait: &wait}).
		WithSupervisor(supervisor)
	svPid := actor.SpawnNamed(svProp, "WatcherSupervisor")
	svPid.Tell(watcher.StartNewWatcherMsg{
		Name: config.Watcher.Name,
		Run:  &config.Watcher.Run,
	})

	sigC := make(chan os.Signal, 1)
	signal.Notify(sigC, os.Interrupt)
	go func() {
		<-sigC
		c.UI.Warn("[SV] [Info] Interrupt cought. Stopping server.")
		svPid.Stop()
	}()

	opts := remoting.WithDialOptions(grpc.WithInsecure(), grpc.FailOnNonTempDialError(true))
	remoting.Start(config.Watcher.Remote, opts)

	return <-wait
}

// Synopsis cli helper
func (c *WatchCommand) Synopsis() string {
	return "Start a watcher instance"
}

// Help cli helper
func (c *WatchCommand) Help() string {
	return "<insert help text here>"
}
