package command

import (
	"local/mechwipf/serverwatcher/client"
	"os"

	"google.golang.org/grpc"

	"io/ioutil"
	"log"

	"fmt"

	console "local/mechwipf/serverwatcher/shared/console"

	"github.com/AsynkronIT/gam/remoting"
	"github.com/mitchellh/cli"
	"google.golang.org/grpc/grpclog"
)

// ClientCommand for whatever
type ClientCommand struct {
	UI cli.Ui
}

// Run something
func (c *ClientCommand) Run(args []string) int {
	grpclog.SetLogger(log.New(ioutil.Discard, "", 0))
	log.SetOutput(ioutil.Discard)

	opts := remoting.WithDialOptions(grpc.WithInsecure(), grpc.FailOnNonTempDialError(true))
	remoting.Start("localhost:0", opts)

	client := client.NewClientToWatcher(args[0])

	con := console.NewConsole(func(text string) {
		if text == "q" {
			client.Close()
			c.UI.Output("Stopping.")
			os.Exit(0)
		}

		client.Send(text)
	})

	con.Command("send", func(text string) {
		fmt.Println(text)
		client.Input(text)
	})

	con.Run()
	return 0
}

// Synopsis cli helper
func (c *ClientCommand) Synopsis() string {
	return "Start a watcher server, watcher instances can connect to this one"
}

// Help cli helper
func (c *ClientCommand) Help() string {
	return "<insert help text here>"
}
