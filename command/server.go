package command

import "github.com/mitchellh/cli"

// ServerCommand for whatever
type ServerCommand struct {
	UI cli.Ui
}

// Run something
func (c *ServerCommand) Run(args []string) int {
	c.UI.Output("Whatever?")
	return 0
}

// Synopsis cli helper
func (c *ServerCommand) Synopsis() string {
	return "Start a watcher server, watcher instances can connect to this one"
}

// Help cli helper
func (c *ServerCommand) Help() string {
	return "<insert help text here>"
}
