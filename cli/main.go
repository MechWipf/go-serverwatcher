package cli

import "github.com/mitchellh/cli"

// Run the main part of the cli
func Run(args []string) int {
	return RunCustom(args, Commands())
}

// RunCustom blablabla
func RunCustom(args []string, commands map[string]cli.CommandFactory) int {
	c := cli.NewCLI("ServerWatcher", "0.0.1")
	c.Args = args
	c.Commands = commands

	exitStatus, err := c.Run()
	if err != nil {
		println(err)
	}

	return exitStatus
}
