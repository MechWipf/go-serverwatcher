package cli

import (
	"local/mechwipf/serverwatcher/command"
	"os"

	"github.com/mitchellh/cli"
)

// Commands is exported
func Commands() map[string]cli.CommandFactory {
	ui := &cli.BasicUi{
		Writer:      os.Stdout,
		ErrorWriter: os.Stderr,
	}

	return map[string]cli.CommandFactory{
		"server": func() (cli.Command, error) {
			return &command.ServerCommand{
				UI: ui,
			}, nil
		},
		"watch": func() (cli.Command, error) {
			return &command.WatchCommand{
				UI: ui,
			}, nil
		},
		"client": func() (cli.Command, error) {
			return &command.ClientCommand{
				UI: ui,
			}, nil
		},
	}
}
