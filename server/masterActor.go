package server

import (
	"local/mechwipf/serverwatcher/messages"

	"github.com/AsynkronIT/gam/actor"
)

// MasterActor for acting as server
type MasterActor struct {
	configPath string
}

func (state *MasterActor) init() {
}

func (state *MasterActor) handleMessage() {

}

// Receive handling all the actor things
func (state *MasterActor) Receive(context actor.Context) {
	switch context.Message().(type) {
	case actor.Started:
		state.init()
	case *messages.Message:
		state.handleMessage()
	}
}

// NewMasterActor returns a new MasterActor
func NewMasterActor(configPath string) *MasterActor {
	return &MasterActor{
		configPath: configPath,
	}
}
