package file

import (
	"local/mechwipf/serverwatcher/server/connector"
	"sync"
)

// Connector is for handling file log writing
type Connector struct {
	m sync.Mutex
	connector.Connector
}

var instance *Connector
var once sync.Once

func (c *Connector) Write() error {
	c.m.Lock()
	defer c.m.Unlock()

	return nil
}

// NewConnector returns a new connector for file logging
func NewConnector() *Connector {
	once.Do(func() {
		instance = &Connector{}
	})
	return instance
}
