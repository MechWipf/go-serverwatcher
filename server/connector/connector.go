package connector

// Connector for uniform database access
type Connector interface {
	Write(int64, int, string) error
}
