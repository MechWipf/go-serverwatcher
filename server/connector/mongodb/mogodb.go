package mongodb

import (
	"fmt"
	"local/mechwipf/serverwatcher/server/connector"
	"sync"
)

// Connector connector for database access
type Connector struct {
	m sync.Mutex
	connector.Connector
}

var instance *Connector
var once sync.Once

// GetConnector returns a singleton
func GetConnector() *Connector {
	once.Do(func() {
		instance = &Connector{}
	})

	return instance
}

func (c *Connector) Write(timestamp int64, level int, message string) error {
	c.m.Lock()
	defer c.m.Unlock()

	fmt.Printf("DB: {\"time\"=%v,\"level\"=%v,\"message\"=%q}", timestamp, level, message)
	return nil
}
