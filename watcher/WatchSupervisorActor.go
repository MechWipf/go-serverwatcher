package watcher

import (
	"os/exec"

	"time"

	"local/mechwipf/serverwatcher/messages"

	"github.com/AsynkronIT/gam/actor"
	"github.com/mitchellh/cli"
)

// WatchSupervisorActor is the supervisor, only one instance of this here should exist,
// it manages cli output and restarting the server watcher
type WatchSupervisorActor struct {
	UI       cli.Ui
	Wait     *chan int
	last     StartNewWatcherMsg
	stopping bool
}

// Receive bla
func (state *WatchSupervisorActor) Receive(context actor.Context) {
	switch msg := context.Message().(type) {
	case *actor.Started:
		if state.last.Name == "" {
			state.UI.Info("[SV] [Info] Started.")
			*state.Wait = make(chan int, 1)
		}
	case *actor.Stopping:
		state.UI.Info("[SV] [Info] Stopping.")
		state.stopping = true
		time.Sleep(5 * time.Second)
	case *actor.Stopped:
		state.UI.Info("[SV] [Info] Stopped.")
		*state.Wait <- 0
	case *actor.Restarting:
		state.UI.Info("[SV] [Info] Restarting.")
	case *messages.ConnectMsg:
		state.UI.Info("[SV] [Info] We got a connection. " + msg.Sender.Host)
		context.Respond(&messages.ConnectResponse{})
	case StartNewWatcherMsg:
		state.UI.Info("[SV] [Info] Start new Watcher.")

		// Prepare command
		cmd := exec.Command(msg.Run.Cmd)
		cmd.Args = msg.Run.Args
		cmd.Dir = msg.Run.Dir
		cmd.Env = msg.Run.Env

		// Prepare and start child
		watchProp := actor.FromInstance(&WatchActor{command: cmd})
		context.SpawnNamed(watchProp, "Watcher")

		state.last = msg
		state.UI.Info("[SV] [Info] Watcher started.")
	case logMsg:
		switch msg.LogType {
		case logInfo:
			state.UI.Info("[Watcher] [Info] " + msg.Message)
		case logWarn:
			state.UI.Warn("[Watcher] [Warn] " + msg.Message)
		case logError:
			state.UI.Error("[Watcher] [Err] " + msg.Message)
		default:
			state.UI.Output(msg.Message)
		}
	case *messages.WMessage:
		context.Self().Tell(logMsg{
			LogType: -1,
			Message: msg.Message,
		})
		context.Respond(&messages.Ok{})
	case doneMsg:
		if msg.Stop {
			context.Self().Stop()
		}

		if !state.stopping && !msg.Stop {
			// We only have one child
			context.Children()[0].Stop()

			state.UI.Info("[SV] [Info] Initialize new Watcher. (3 sec delay)")
			time.Sleep(3 * time.Second)
			// Then start a new child
			context.Self().Tell(state.last)
		}
	}
}
