package watcher

import (
	"io/ioutil"

	"fmt"

	"github.com/hashicorp/hcl"
)

// Config for the watcher
type Config struct {
	Watcher ConfWatcher
}

// ConfWatcher is the actual Watcher config
type ConfWatcher struct {
	Name   string
	Remote string
	Run    ConfRun
}

// ConfRun is for running commandos via exec
type ConfRun struct {
	Cmd  string
	Args []string
	Dir  string
	Env  []string
}

// LoadConfig loads the configuration at the given path
func LoadConfig(path string) (*Config, error) {
	file, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	obj, err := hcl.ParseBytes(file)
	if err != nil {
		return nil, err
	}

	var result Config
	if err := hcl.DecodeObject(&result, obj); err != nil {
		return nil, err
	}

	if result.Watcher.Run.Cmd == "" {
		return nil, fmt.Errorf("No watcher.run.Cmd specified")
	}

	if result.Watcher.Run.Dir == "" {
		return nil, fmt.Errorf("No watcher.run.Dir specified")
	}

	return &result, nil
}
