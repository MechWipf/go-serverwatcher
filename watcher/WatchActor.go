package watcher

import (
	"bufio"
	"io"

	"os/exec"

	"local/mechwipf/serverwatcher/messages"

	"fmt"

	"time"

	"github.com/AsynkronIT/gam/actor"
	"gopkg.in/fatih/set.v0"
)

// WatchActor Actor
type WatchActor struct {
	command *exec.Cmd
	stdin   io.WriteCloser
	clients *set.Set
}

func (state *WatchActor) started(context actor.Context) {

	state.clients = set.New(context.Parent())

	stdout, err := state.command.StdoutPipe()
	if err != nil {
		context.Parent().Tell(logMsg{
			LogType: logError,
			Message: "#started-stdout-pipe " + err.Error(),
		})

		// this one is serious, bubble it up
		panic(err)
	}

	stdin, err := state.command.StdinPipe()
	if err != nil {
		context.Parent().Tell(logMsg{
			LogType: logError,
			Message: "#started-stdin-pipe " + err.Error(),
		})

		// this one is serious, bubble it up
		panic(err)
	}

	state.stdin = stdin

	go reader(state.clients, context, stdout)

	if err := state.command.Start(); err != nil {
		context.Parent().Tell(logMsg{
			LogType: logError,
			Message: "#started-start " + err.Error(),
		})

		// this one is serious, bubble it up
		panic(err)
	}
}

func (state *WatchActor) stopping(context actor.Context) {
	if err := state.command.Process.Kill(); err != nil {
		context.Parent().Tell(logMsg{
			LogType: logWarn,
			Message: "Error while stopping " + err.Error(),
		})
	}
}

func (state *WatchActor) stopped(context actor.Context) {
	context.Parent().Tell(logMsg{
		LogType: logInfo,
		Message: "Watcher stopped.",
	})

}

func (state *WatchActor) handleInput(context actor.Context, msg *messages.WInput) {
	sender := msg.GetSender()

	io.WriteString(state.stdin, msg.Message+"\n")

	sender.Tell(&messages.WInputResponse{
		Message: "Input received.",
	})

	context.Parent().Tell(logMsg{
		LogType: logInfo,
		Message: fmt.Sprintf("%v: %v", sender.Host, msg.Message),
	})

	context.Respond(&messages.Ok{})
}

func (state *WatchActor) handleSend(context actor.Context, msg *messages.WSend) {
	sender := msg.GetSender()
	var message string
	context.Respond(&messages.Ok{})

	switch msg.Message {
	case "restart":
		message = "Restarting gameserver."
		context.Self().Stop()
	case "stop":
		message = "Stopping gameserver."
		context.Parent().Tell(doneMsg{Stop: true})
	default:
		message = "Unknown command."
	}

	sender.Tell(&messages.WSendResponse{
		Message: message,
	})

}

func (state *WatchActor) handleConnect(context actor.Context, msg *messages.ConnectMsg) {
	sender := msg.GetSender()

	dbglist := state.clients.List()
	_ = dbglist

	has := false
	for _, item := range dbglist {
		client := item.(*actor.PID)
		if client.Host == sender.Host && client.Id == sender.Id {
			has = true
			break
		}
	}

	if !has {
		state.clients.Add(sender)
		context.Parent().Tell(logMsg{
			LogType: logInfo,
			Message: fmt.Sprintf("%v connected.", sender.Host),
		})
	}

	context.Respond(&messages.Ok{})
}

// Receive processes messages
func (state *WatchActor) Receive(context actor.Context) {
	switch msg := context.Message().(type) {
	case *actor.Started:
		state.started(context)
	case *actor.Stopping:
		state.stopping(context)
	case *actor.Stopped:
		state.stopped(context)
	case *messages.WInput:
		state.handleInput(context, msg)
	case *messages.WSend:
		state.handleSend(context, msg)
	case *messages.ConnectMsg:
		state.handleConnect(context, msg)
	}
}

func reader(clients *set.Set, context actor.Context, pipe io.Reader) {
	reader := bufio.NewScanner(pipe)

	for reader.Scan() {
		text := reader.Text()
		go send(clients, text)
	}

	if err := reader.Err(); err != nil {
		// this one is serious, bubble it up
		panic(err)
	}

	context.Parent().Tell(logMsg{
		LogType: logWarn,
		Message: "Process ended; Stopping Watcher.",
	})

	context.Parent().Tell(doneMsg{Stop: false})
}

func send(clients *set.Set, message string) {
	msg := &messages.WMessage{
		Message: message,
	}

	for _, tmp := range clients.List() {
		client := tmp.(*actor.PID)
		result := client.RequestFuture(msg, 250*time.Millisecond)
		if _, err := result.Result(); err != nil {
			clients.Remove(client)
		}
	}
}
