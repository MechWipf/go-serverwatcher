package watcher

// StartNewWatcherMsg for starting a watcher
type StartNewWatcherMsg struct {
	Name string
	Run  *ConfRun
}

type logStatus int

const (
	logInfo  logStatus = iota
	logWarn            = iota
	logError           = iota
)

type logMsg struct {
	LogType logStatus
	Message string
}

type doneMsg struct {
	Stop bool
}
