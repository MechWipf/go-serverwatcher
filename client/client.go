package client

import (
	"fmt"
	"local/mechwipf/serverwatcher/messages"

	"time"

	"runtime"

	"github.com/AsynkronIT/gam/actor"
)

type clientActor struct{}

func (c *clientActor) Receive(context actor.Context) {
	switch msg := context.Message().(type) {
	case *messages.WInputResponse:
		// fmt.Println(msg.Message)
	case *messages.WSendResponse:
		fmt.Println(msg.Message)
	case *messages.ConnectResponse:
		fmt.Println("Connected.")
	case *messages.WMessage:
		fmt.Printf("[Process] %v \n", msg.Message)
		context.Respond(&messages.Ok{})
	}
}

type clientToWatcherWrapper struct {
	actor     *actor.PID
	receiver  *actor.PID
	connected bool
}

func NewClientToWatcher(con string) *clientToWatcherWrapper {
	prop := actor.FromInstance(&clientActor{})
	pid := actor.Spawn(prop)

	watcher := actor.NewPID(con, "Watcher")

	client := &clientToWatcherWrapper{
		actor:    pid,
		receiver: watcher,
	}

	go func() {
		var retry time.Duration = 1
		for true {
			if !client.connected {
				request := watcher.RequestFuture(&messages.ConnectMsg{
					Sender: pid,
				}, 1*time.Second)

				if _, err := request.Result(); err != nil {
					fmt.Printf("Watcher(%v) not responding.\n", watcher.Host)
					client.connected = false
					retry++
				} else {
					client.connected = true
					retry = 0
				}

				_time := retry * time.Second * 2
				if _time > 30*time.Second {
					_time = 30 * time.Second
				}
				time.Sleep(_time)
			}
			runtime.Gosched()
		}
	}()

	go func() {
		for true {
			if client.connected {
				request := watcher.RequestFuture(&messages.ConnectMsg{
					Sender: pid,
				}, 1*time.Second)

				if _, err := request.Result(); err != nil {
					fmt.Printf("Watcher(%v) not responding.\n", watcher.Host)
					client.connected = false
				} else {
					client.connected = true
				}
			}
			time.Sleep(10 * time.Second)
		}
	}()

	return client
}

func (c *clientToWatcherWrapper) Send(msg string) {
	request := c.receiver.RequestFuture(&messages.WSend{
		Sender:  c.actor,
		Message: msg,
	}, 1*time.Second)

	go func() {
		if _, err := request.Result(); err != nil {
			fmt.Printf("Watcher(%v) not responding.\n", c.receiver.Host)
			c.connected = false
		}
	}()
}

func (c *clientToWatcherWrapper) Input(msg string) {
	request := c.receiver.RequestFuture(&messages.WInput{
		Sender:  c.actor,
		Message: msg,
	}, 1*time.Second)

	go func() {
		if _, err := request.Result(); err != nil {
			fmt.Printf("Watcher(%v) not responding.\n", c.receiver.Host)
			c.connected = false
		}
	}()
}

func (c *clientToWatcherWrapper) Close() {
	c.actor.Stop()
}
