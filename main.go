package main

import (
  "os"
  "local/mechwipf/serverwatcher/cli"
)

func main() {
  os.Exit(cli.Run(os.Args[1:]))
}