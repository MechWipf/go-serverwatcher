watcher {
  Name = "Minecraft 1.11"

  run {
    // This needs to be an absolute path
    Cmd = "C:\\Program Files\\Java\\jdk1.8.0_112\\jre\\bin\\java.exe"
    Dir = "D:\\Server\\Minecraft"

    Args = [
      "-server",
      "-Xmx1024M",
      "-Xms1024M",
      "-jar",
      "minecraft_server.jar",
      "nogui",
    ]
  }

  Remote = "[::1]:8091"
  MessageBroker = "[::1]:8090"
}
