watcher {
  Name = "Vanilla Starbound"

  run {
    Cmd  = "D:/Program Files (x86)/Steam/steamapps/common/Starbound/win64/starbound_server.exe"
    Dir  = "D:/Program Files (x86)/Steam/steamapps/common/Starbound/win64/"
  }

  Remote = "[::1]:8091"
  MessageBroker = "[::1]:8090"
}
