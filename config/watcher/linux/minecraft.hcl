watcher {
  Name = "Minecraft 1.11"

  run {
    Cmd = "java"
    Dir = "/mnt/d/Server/Minecraft"

    Args = [
      "-server",
      "-jar",
      "minecraft_server.jar",
      "nogui",
    ]

    Env = [
      "PATH=/usr/bin/"
    ]
  }

  Remote = "[::1]:8091"
  MessageBroker = "[::1]:8090"
}
