watcher {
  Name = "Vanilla Starbound"

  run {
    Cmd  = "/mnt/d/Program Files (x86)/Steam/steamapps/common/Starbound/linux/starbound_server"
    Dir  = "/mnt/d/Program Files (x86)/Steam/steamapps/common/Starbound/linux/"
  }

  Remote = "[::1]:8091"
  MessageBroker = "[::1]:8090"
}
