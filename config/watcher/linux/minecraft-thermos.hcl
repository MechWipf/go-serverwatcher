watcher {
  Name = "Minecraft 1.11"

  run {
    Cmd = "java"
    Dir = "/mnt/d/Server/Minecraft - Thermos"

    Args = [
      "-XX:+UseG1GC",
      "-XX:+UseFastAccessorMethods",
      "-XX:+OptimizeStringConcat",
      "-XX:+AggressiveOpts",
      "-XX:MaxGCPauseMillis=10",
      "-Xms2G",
      "-Xmx3G",
      "-XX:hashCode=5",
      "-Dfile.encoding=UTF-8",
      "-jar",
      "Thermos-1.7.10-1614-server.jar"
    ]
  }

  Env = [
    "PATH=/usr/bin/"
  ]

  Remote = "[::1]:8091"
  MessageBroker = "[::1]:8090"
}
